import FlickityFull from "flickity-fullscreen";
import * as rxjs from "rxjs";
import { startButtonHandler, skipButtonHandler, playButtonHandler} from "./eventHandlers";
import $ from "jquery";

export const flkt = new FlickityFull(".main-carousel", {
  cellAlign: "center",
  fullscreen: true,
  draggable: false,
  prevNextButtons: false,
  pageDots: false
});

const playButton = document.getElementById("play-button");
rxjs.fromEvent(playButton, "click")
.subscribe(event => playButtonHandler(event));

const initialButton = document.getElementById("initial-button");
rxjs.fromEvent(initialButton, "click")
.subscribe(event => startButtonHandler(event, flkt));

const skipButton = document.getElementById("skip-button");
rxjs.fromEvent(skipButton, "click")
.subscribe(event => skipButtonHandler(event, flkt));
