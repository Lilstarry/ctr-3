import $ from "jquery";
import { setTimer } from "./timer"
export const startButtonHandler = (event, sliderObject) => {
    event.preventDefault();
    event.stopPropagation();
    if (sliderObject) {
        setTimer();
        sliderObject.next();
    } else {
        throw Error('Slider not defined');
    }
}

export const skipButtonHandler = (event, sliderObject) => {
    event.preventDefault();
    event.stopPropagation();
    if (sliderObject) {
        sliderObject.next();
    } else {
        throw Error('Slider not defined');
    }
    $("#ctr-video")[0].currentTime = 0;
    $("#ctr-video")[0].pause();
}


export const playButtonHandler = (event) => {
    event.preventDefault();
    event.stopPropagation();
    $("#ctr-video")[0].play();
    $("#play-wrap")[0].style.opacity = 0;
}
