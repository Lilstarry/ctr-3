const SEC = 1000;
const timeOut = 60 * SEC;
let result = 0;

const timerId = setInterval(() => {
  result++;
  let display = "00:" + (60 - result < 10 ? "0" : "") + String(60 - result);
  document.getElementById("counter").innerText = display;
}, SEC);

export const setTimer = () => {
  // last slide?
  result = 0;
  setTimeout(() => {
  clearInterval(timerId);
  }, timeOut);
};
